<?php

/* 
 * Page for displaying a retro board.
 */
include_once $_SERVER["DOCUMENT_ROOT"] . '/catalyteretro/engine.php';
unset($_SESSION["error"]); // removal of any out of date error messages.

$_SESSION["boardId"] = $_GET["boardId"];
?>

<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>                
        <script src="/catalyteretro/js/retroJS.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript">
            
    var latestComment = "";

    var source = new EventSource("latestComment");
    var voteSource = new EventSource("latestVoteCount");
    
    //HTML SSE for updating the board with latest comments.
    source.onmessage = function(event) {
        var data = JSON.parse(event.data);
        
        //Check to ensure last comment does not get loaded twice on page load.
        if (latestComment === ""){
            latestComment = data.msg;
        }
        //Check to prevent last comment from continually being repeated.
        if(latestComment != data.msg){
            var html = "<li>"+data.msg+"<div class='rating'><i class='button rating-up fa fa-thumbs-o-up' aria-hidden='true' onclick='upVote("+data.commentId+")'></i><span class='counter' id='com"+data.commentId+"'>0</span></div></li>";
            document.getElementById(data.id+"List").innerHTML += html; //Assigns comments only to the board it belongs to.
            latestComment = data.msg;
            
            //This will be used to update the live votes.
            commentId = data.commentId;
            //Send ajax call to back end to add comment data to Session array.
            $.ajax({
                type: "POST",
                url: "addToVoteArray",
                data: {cId: commentId},
                success: function (e) {   
//                    console.log(e);
               }
            });
            
        }

    };
    
    
    voteSource.onmessage = function(event) {
        var data = JSON.parse(event.data);
        
        //iterate over the json data parsed and update the vote counts on the page. 
        var votesArray = data.msg;
        if(votesArray.length !== 0){
            for(var i in votesArray){
                document.getElementById("com"+i).innerHTML = votesArray[i];
            } 
        }
       
    };
            
    
    //HTML5 event for updating vote counts on the page.
    $(document).ready(function(){

        /**
         * Allows for form to be submitted and comment to be posted on pressing of enter key.
         */
        $(function() {
           $(".textEntry").keypress(function (e) {
               if(e.which == 13) {
                   //submit form via ajax, this is not JS but server side scripting so not showing here
                   $(this).parent("form").submit();
                   e.preventDefault();
               }
           });
       });

        /**
         * AJAX call to submit the comment form to the backend.
         */
       $("form").submit(function(event){
           columnName = this.name;
           columnId = $("#"+columnName+"Id").val();
           boardId = $("#"+columnName+"BoardId").val();
           comment = $("#"+columnName+"Comment").val().trim();
           event.preventDefault();//prevents actual form submission
           //Prevent submission of blank comments.
           if(comment != null && comment!= ""){
           
                $("#"+columnName+"Comment").val("");
                userSubmission = true;
                

                $.ajax({
                      type: "POST",
                      url: "createComment",
                      data: {columnId: columnId, boardId: boardId, comment: comment, userSubmission: userSubmission},
                      success: function (e) {                      
                         if(!e){
                             alert("This board has been locked by the owner.  No new comments may be added at this time.");
                         }
                     }
               });
            }          
       });
    });       
    
   
    

        </script>
        <style>
            /* Create three equal columns that floats next to each other */
.column {
    float: left;
    width: 31%;
    /*padding: 10px;*/
    height: 100%; /* Should be removed. Only for demonstration */
    background-color:#54DEFF; 
    border-style: solid;
    text-align: center;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
    text-align: center;
}

li{
    background-color:#FFA500; 
    border-style: solid;
    max-width: 80%;
    text-align: left;
    padding: 10px;
}

.textEntry{
    max-width: 75%;
}


#navDiv{
    text-align: right;
}

.button {
    cursor: pointer;
    display: inline-block;
    background: grey;
    color: #fff;
    /*padding: 20px 30px;*/
    transition: all 0.75s ease;

    &.rating-up:hover {
        background: limegreen;
    }	
		
}

.rating{
    text-align: right;
}

.counter {
    /*padding: 17px;*/
    /*border: 2px solid grey;*/
    display: inline-block;
    vertical-align: top;
}


        </style>
    </head>
    <body>
        <div id="navDiv">
            <a href="dashboard">Dashboard</a>   
            <button id="copyLinkBtn" onclick="copyLink()" style="float: left;">Copy Board URL</button>
        </div>
            
        </div>
        <br>
        <div class="row">
            <?php 
                $column = new column();
                
                $boardId = $_GET["boardId"];
                
                $_SESSION["commentsArray"] = array(); //Create a new array to hold and update comments and votes in the session on page load.
                
                $board = new board();
                $board->setId($boardId);
                $board->getBoardById();
                
                $column->setBoardId($boardId);
                $columns = $column->getColumnsByBoardId();
                
                $comment = new comment();
                
                foreach ($columns as $c){
                    echo "<div class='column'>";
                        echo "<h2>".$c->getName()."</h2>";

                            //Simple form to add data.
                            echo "<form name='".$c->getId()."' id='".$c->getId()."Form' autocomplete='off'>";
                            
                            if($board->getLocked()){ //Make textarea readonly for a locked board.
                                echo "<textarea class='textEntry' rows='4' cols='40' id='".$c->getId()."Comment' readonly>This board is read only.</textarea>"; 
                            }else{
                                echo "<textarea class='textEntry' rows='4' cols='40' id='".$c->getId()."Comment'></textarea>"; 
                            }
                            
                            echo "<input type='hidden' id='".$c->getId()."Id' value='".$c->getId()."'/>";
                            echo "<input type='hidden' id='".$c->getId()."BoardId' value='".$c->getBoardId()."'/>";
                            echo "</form>";
                            
                            echo "<div id='".$c->getId()."Div'>";
                            echo "<ol id='{$c->getId()}List'>";
                            
                            //Iterate over comments stored in the db.
                            $comment->setBoardId($c->getBoardId());
                            $comment->setColumnId($c->getId());
                            $comments = $comment->getAllCommentsByBoardAndColumn();
                            
                            if($comments != false){  //Check for no comments existing in column.
                                foreach($comments as $com){
                                    $_SESSION["commentsArray"][] = $com;
                                    echo "<li>".$com->getComment();
                                    echo " <div class='rating'>";
                                    echo " <i class='button rating-up fa fa-thumbs-o-up' aria-hidden='true' onclick='upVote(".$com->getId().")'></i>";
                                    echo " <span class='counter' id='com".$com->getId()."'>".$com->getVoteCount()."</span></div>";
                                    echo"</li>";
                                }
                            }
                            
                            
                            echo "</ol>";
                            echo "</div>";
                    
                    echo "</div>";
                }
            ?>
           
        </div>
    </body>
</html>
