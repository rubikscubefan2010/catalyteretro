<?php

/* 
 * Basic login page.
 */
include_once $_SERVER["DOCUMENT_ROOT"] . '/catalyteretro/engine.php';

?>
<html>
    <head>
        <script src="https://apis.google.com/js/platform.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="/catalyteretro/js/retroJS.js"></script>
        <script type="text/javascript">
    $(document).ready(function(){
        gapi.load('auth2', function() { 
            gapi.auth2.init();
            // Ready. 
            auth2 = gapi.auth2.init();
            auth2.attachClickHandler('signinButton', null, onSignIn);
        });        
    });
      
      
    
        </script>
        
        <style>
            body{
                background-color: #54DEFF;
            }
            
            .g-signin2{
                text-align: center;
            }
        </style>
        <meta name="google-signin-client_id" content="421384137401-q65iat5dmc4qcg7dvjckh8hbivoaaoe8.apps.googleusercontent.com">
    </head>
    <body>
        
        <div class="g-signin2" id="signinButton"></div>
        
        
        <br>
        
    </body>
</html>