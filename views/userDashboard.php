<?php

/* 
 * Page for displaying user accessible features such as boards and board creation.
 */
include_once $_SERVER["DOCUMENT_ROOT"] . '/catalyteretro/engine.php';

isLoggedIn();
?>

<html>
    <head>
        <script src="https://apis.google.com/js/platform.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>        
        <script src="/catalyteretro/js/retroJS.js"></script>
        <script type="text/javascript">
            
           $(document).ready(function(){
                gapi.load('auth2', function() { //Load the google api
                    gapi.auth2.init();
                    // Ready. 
                    auth2 = gapi.auth2.init();
                }); 
            });
            
            
        </script>
        <style>
            body{
                background-color: #54DEFF;
            }
            
            form{
                text-align: left;
            }
            
            #formDiv{
                text-align: right;
            }
            
            #navDiv{
                text-align: right;
            }
        </style>
        <meta name="google-signin-client_id" content="421384137401-q65iat5dmc4qcg7dvjckh8hbivoaaoe8.apps.googleusercontent.com">
    </head>
    <body>
        <div  ng-app="app" ui-view></div>
        <div id="navDiv">
            
            <button id="newBoard" onclick="showBoardForm()" style="float: left;">Create New Board</button>  <button id="signoutBtn" onclick="signOut();">Sign out</button>
        </div>
        
        <br>
        
        
        
        <div id="formDiv" style="display:none">
            <form name="newBoardForm" id='newBoardForm' onsubmit="return validateBoardForm()" action='createBoard' method='post' autocomplete="off">
                <input type='text' name='boardName' id='boardName' required /><label>Board Name*</label><br>
                <input type='text' name='firstColumn' id='firstColumn' required /><label>First Column Name*</label><br>
                <input type='text' name='secondColumn' id='secondColumn' required /><label>Second Column Name*</label><br>
                <input type='text' name='thirdColumn' id='thirdColumn' required /><label>Third Column Name*</label><br>
                <input type='submit' value='Submit'/>
            </form>
        </div>
        
        <div id='myBoards'>
            My Boards:
            <ul>
                <?php 
                    $board = new board();
                    
                    $boards = $board->getAllBoardsByUserId($_SESSION['user']);
                    if(!$boards){
                        echo "You have not created any boards yet.";
                        
                    }else{
                        foreach($boards as $myBoard){

                            echo "<li><a href='board?boardId=".$myBoard->getId()."'>".$myBoard->getName()."</a> ";
                            if($myBoard->getLocked()){
                                echo "<button id='".$myBoard->getId()."' onclick='updateLockedState(id)'>Unlock</button></li>";    
                            }else{
                                echo "<button id='".$myBoard->getId()."' onclick='updateLockedState(id)'>Lock</button></li>";
                            }
                            
                        }
                    }
                    
                ?>
            </ul>
        </div>
             
    </body>
</html>
