<?php

/*
 * Model class for user object.
 */
include_once $_SERVER["DOCUMENT_ROOT"] . '/catalyteretro/engine.php';


/**
 * Description of user
 *
 * @author rWinkler
 */
class user {
    private $id;
    private $fname;
    private $lname;
    private $password;
    private $email;
    
    /**
     * Returns id for user.
     * @return int
     */
    function getId(){
        return $this->id;
    }
    
    /**
     * Returns email for user
     * @return string
     */
    function getEmail(){
        return $this->email;
    }
    
    /**
     * Returns first name
     * @return string
     */
    function getFname(){
        return $this->fname;
    }
    
    /**
     * Returns last name
     * @return string
     */
    function getLname(){
        return $this->lname;
    }
    
    /**
     * Set password for user
     * @param string
     */
    function setPassword($x){
        $this->password = $x;
    }
    
    /**
     * Set email for user
     * @param string
     */
    function setEmail($x){
        $this->email = $x;
    }
    
    /**
     * Set first name for user
     * @param string
     */
    function setFname($x){
        $this->fname = $x;
    }
    
    /**
     * Set last name for user
     * @param string
     */
    function setLname($x){
        $this->lname = $x;
    }
    
    /**
     * Creates a user in the db
     * @global mysqli connection $mysqli
     */
    function create(){
        global $mysqli;  
        $stmt = $mysqli->prepare("INSERT INTO catalyteretro_user (email) VALUES (?)");
        $stmt->bind_param("s", $this->email);
        $stmt->execute();
        $stmt->close();
        
    }
    
    /**
     * Searches for user's email in database.
     * Since we login with Google, if no user is in the db, this creates one on the fly.
     * TODO: Create proper returns for no user and multiple users returned.
     */
    function login(){
        global $mysqli; 
        
        $stmt = $mysqli->prepare("SELECT id FROM catalyteretro_user WHERE email = ?");
        $stmt->bind_param("s", $this->email);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        
        if(mysqli_num_rows($result) == 1){ //There should only be one row returned.
            $row = $result->fetch_assoc();
            $this->id = $row["id"];
            return true;            
        }elseif(mysqli_num_rows($result) == 0){ //No user returned
            //create user
            $this->create();
            return true;
        }else{ //More than one user returned
            return false;
        }       
    }
    
    
}
