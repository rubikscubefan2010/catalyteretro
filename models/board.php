<?php

/*
 * Model class for retro board object.
 */

/**
 * Description of board
 *
 * @author rWinkler
 */
class board {
    private $id;
    private $name;
    private $userId;
    private $locked;
    
    /**
     * Returns the board id.
     * @return int
     */
    function getId(){
        return $this->id;
    }
    
    /**
     * Returns the name of the board.
     * @return string
     */
    function getName(){
        return $this->name;
    }
    
    /**
     * Returns the id of the user owner of the retro board.
     * @return int
     */
    function getUserId(){
        return $this->userId;
    }
    
    /**
     * Returns the status of a board's locked state.
     * @return bool
     */
    function getLocked(){
        return $this->locked;
    }
    
    /**
     * Sets the id of the board.
     * @param int
     */
    function setId($x){
        $this->id = $x;
    }
    
    /**
     * Sets the name of the board.
     * @param string
     */
    function setName($x){
        $this->name = $x;
    }
    
    /**
     * Sets the user owner id of the board.
     * @param int
     */
    function setUserId($x){
        $this->userId = $x;
    }
    
    /**
     * Sets the locked state of the board.
     * @param bool
     */
    function setLocked($x){
        $this->locked = $x;
    }


    /**
     * Creates a board in the db 
     * @global mysqli connection $mysqli
     * @return Nothing on success.
     * @return bool FALSE on failure.
     */
    function create(){
        global $mysqli;
        
        $stmt = $mysqli->prepare("INSERT INTO catalyteretro_board (name, userId) VALUES (?, ?)");
        $stmt->bind_param("si", $this->name, $this->userId);
        $stmt->execute();
        $stmt->close();
        
        $stmt2 = $mysqli->prepare("SELECT id FROM catalyteretro_board WHERE userId = ? ORDER BY id DESC LIMIT 1");
        $stmt2->bind_param("i", $this->userId);
        $stmt2->execute();
        $result = $stmt2->get_result();
        $stmt2->close();
        
        if(mysqli_num_rows($result) == 1){ //There should only be one row returned.
            $row = $result->fetch_assoc();
            $this->id = $row["id"];
        }elseif(mysqli_num_rows($result) == 0){ //No user returned
            return false;
        }else{ //More than one user returned
            return false;
        }
        
    }
    
    /**
     * Sets the name of the board based on the board's id.
     * @global mysqli connection $mysqli
     * @return boolean FALSE on failure
     */    
    function getBoardById(){
        global $mysqli;
        
        $stmt = $mysqli->prepare("SELECT name, userId, locked FROM catalyteretro_board WHERE id = ?");
        $stmt->bind_param("i", $this->id);
        $stmt->execute();        
        $result = $stmt->get_result();
        $stmt->close();
        
        if(mysqli_num_rows($result) == 1){ //There should only be one row returned.
            $row = $result->fetch_assoc();
            $this->name = $row["name"];
            $this->userId = $row["userId"];
            $this->locked = $row["locked"];
        
        }elseif(mysqli_num_rows($result) == 0){ //No user returned
            return false;
        }else{ //More than one user returned
            return false;
        }
    }
    
    /**
     * Returns all boards in the db belonging to a specific user.
     * @global mysqli connection $mysqli
     * @param int $userId
     * @return boolean|\board
     */
    function getAllBoardsByUserId($userId){
        $boards = array();
        global $mysqli;
        
        $stmt = $mysqli->prepare("SELECT id, name, locked FROM catalyteretro_board WHERE userId = ?");
        $stmt->bind_param("i", $userId);
        $stmt->execute();        
        $result = $stmt->get_result();
        $stmt->close();
        
        if(mysqli_num_rows($result) > 0){
            while($row = $result->fetch_assoc()) {
                $board = new board();
                $board->setId($row["id"]);
                $board->setName($row["name"]);
                $board->setLocked($row["locked"]);
                $board->setUserId($userId);
                $boards[] = $board;
            }
            return $boards;
        }else{
            return false;
        }
        
    }
    
    /**
     * Compares a name for a retro board to the rest of the user's board names.
     * @global mysqli connection $mysqli
     * @return boolean
     */
    function uniqueBoardNameCheck(){
        global $mysqli;  
        $stmt = $mysqli->prepare("SELECT id FROM catalyteretro_board WHERE name = ? AND userId = ?");
        $stmt->bind_param("si", $this->name, $this->userId);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        
        if(mysqli_num_rows($result) > 0){ //Email already exists.
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Updates the locked state of the board into the db.
     * @global mysqli connection $mysqli
     */
    function boardLock(){
        global $mysqli;
        $locked = $this->locked; //Setting this here to avoid pass by reference error.
        $id = $this->id; //Setting this here to avoid pass by reference error.
        $stmt = $mysqli->prepare("UPDATE catalyteretro_board SET locked = ? WHERE id = ?");
        $stmt->bind_param("ii", $locked, $id);
        $stmt->execute();
        $stmt->close();
    }
}
