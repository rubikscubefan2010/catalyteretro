<?php

/*
 * Model class for the comment object, a child of the column object.
 */

/**
 * Description of comment
 *
 * @author rWinkler
 */
class comment {
    private $id;
    private $comment;
    private $boardId;
    private $columnId;
    private $voteCount;
    
    /**
     * Returns the id of the comment.
     * @return int
     */
    function getId(){
        return $this->id;
    }
    
    /**
     * Returns the text of the comment.
     * @return string
     */
    function getComment(){
        return $this->comment;
    }
    
    /**
     * Returns the id of the board the comment belongs to.
     * @return int
     */
    function getBoardId(){
        return $this->boardId;
    }
    
    /**
     * Returns the id of the column the comment belongs to.
     * @return int
     */
    function getColumnId(){
        return $this->columnId;
    }
    
    /**
     * Returns the current vote count of a comment.
     * @return int
     */
    function getVoteCount(){
        return $this->voteCount;
    }
    
    /**
     * Sets the id of the comment object.
     * @param int
     */
    function setId($x){
        $this->id = $x;
    }
    
    /**
     * Sets the text of the comment object.
     * @param string
     */
    function setComment($x){
        $this->comment = $x;
    }
    
    /**
     * Sets the id of the board the comment belongs to.
     * @param int
     */
    function setBoardId($x){
        $this->boardId = $x;
    }
    
    /**
     * Sets the id of the column the comment belongs to.
     * @param int
     */
    function setColumnId($x){
        $this->columnId = $x;
    }
    
    /**
     * Sets the vote count for comment.
     * @param int $x
     */
    function setVoteCount($x){
        $this->voteCount = $x;
    }
    
    /**
     * Creates a new comment object in the db.
     * @global mysqli connection $mysqli
     */
    function create(){
        global $mysqli;
        
        $stmt = $mysqli->prepare("INSERT INTO catalyteretro_comment (comment, columnId, boardId) VALUES (?, ?, ?)");
        $stmt->bind_param("sii", $this->comment, $this->columnId, $this->boardId);
        $stmt->execute();
        $stmt->close();
    }
    
    /**
     * Returns all comments for a specific column and board.
     * @global type $mysqli
     * @return array of comments
     * @return boolean
     */
    function getAllCommentsByBoardAndColumn(){
        global $mysqli;
        $comments = array();
        
        $stmt = $mysqli->prepare("SELECT * FROM catalyteretro_comment WHERE boardId = ? AND columnId = ?");
        $stmt->bind_param("ii", $this->boardId, $this->columnId);
        $stmt->execute();        
        $result = $stmt->get_result();
        $stmt->close();
        
        if(mysqli_num_rows($result) > 0){
            while($row = $result->fetch_assoc()) {
                $c = new comment();
                $c->setId($row["id"]);
                $c->setVoteCount($row["voteCount"]);
                $c->setComment($row["comment"]);
                $c->setBoardId($row["boardId"]);
                $c->setColumnId($row["columnId"]);
                $comments[] = $c;
            }
            return $comments;
        }else{
            return false;
        }
    }
    
    
    
    /**
     * Returns all comments and vote counts and ids for a specific board.
     * @global type $mysqli
     * @return array of comments
     * @return boolean
     */
    function getAllCommentsByBoard(){
        global $mysqli;
        $comments = array();
        
        $stmt = $mysqli->prepare("SELECT * FROM catalyteretro_comment WHERE boardId = ?");
        $stmt->bind_param("i", $this->boardId);
        $stmt->execute();        
        $result = $stmt->get_result();
        $stmt->close();
        
        if(mysqli_num_rows($result) > 0){
            while($row = $result->fetch_assoc()) {
                $c = new comment();
                $c->setId($row["id"]);
                $c->setVoteCount($row["voteCount"]);
                $c->setComment($row["comment"]);
                $c->setBoardId($row["boardId"]);
                $c->setColumnId($row["columnId"]);
                $comments[] = $c;
            }
            return $comments;
        }else{
            return false;
        }
    }
    
    /**
     * Returns the last comment in the db for a specific board.
     * @global mysqli connection $mysqli
     * @return  comment
     * @return boolean
     */
    function getLatestComment(){
        global $mysqli;
        
        $stmt = $mysqli->prepare("SELECT id, comment, columnId FROM catalyteretro_comment WHERE boardId = ? ORDER BY id DESC LIMIT 1");
        $stmt->bind_param("i", $_SESSION["boardId"]);
        $stmt->execute();        
        $result = $stmt->get_result();
        $stmt->close();
        
        //Maybe return an array here by comparing to the previous last comment id to avoid the race condition.
        if(mysqli_num_rows($result) > 0){
            $row = $result->fetch_assoc();
            $this->columnId = $row["columnId"];
            $this->comment = $row["comment"];
            $this->id = $row["id"];
        }else{
            //This prevents the JSON from breaking when loading a blank board.
            $this->columnId = 0;
            $this->comment = 0;
            $this->id = 0;
        }
    }
    
    /**
     * Updates the vote count of a comment in the db.
     * @global mysqli connection $mysqli
     */
    function updateVoteCount(){
        global $mysqli;
        
        $stmt = $mysqli->prepare("UPDATE catalyteretro_comment SET voteCount = ? WHERE id = ?");
        $stmt->bind_param("ii", $this->voteCount, $this->id);
        $stmt->execute();
        $stmt->close();
    }
    
    
    function getCommentById(){
        global $mysqli;
        
        $stmt = $mysqli->prepare("SELECT * FROM catalyteretro_comment WHERE id = ?");
        $stmt->bind_param("i", $this->id);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        
        //Proces the comment.
        if(mysqli_num_rows($result) == 1){ //There should only be one row returned.
            $row = $result->fetch_assoc();
            $this->comment = $row["comment"];
            $this->boardId = $row["boardId"];
            $this->columnId = $row["columnId"];
            $this->voteCount = $row["voteCount"];
        
        }elseif(mysqli_num_rows($result) == 0){ //No comment returned
            return false;
        }else{ //More than one comment returned
            return false;
        }
    }
}
