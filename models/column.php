<?php

/*
 * Model class for column object, a child of board.
 */

/**
 * Description of column
 *
 * @author rWinkler
 */
class column {
    private $id;
    private $name;
    private $boardId;
    
    /**
     * Returns the id of the column
     * @return int
     */
    function getId(){
        return $this->id;
    }
    
    /**
     * Returns the name of the column.
     * @return string
     */
    function getName(){
        return $this->name;
    }
    
    /**
     * Returns the id of the board the column belongs to.
     * @return int
     */
    function getBoardId(){
        return $this->boardId;
    }
    
    /**
     * Sets the id of the column object.
     * @param int
     */
    function setId($x){
        $this->id = $x;
    }
    
    /**
     * Sets the name of the column object.
     * @param string
     */
    function setName($x){
        $this->name = $x;
    }
    
    /**
     * Sets the id of the board the column object belongs to.
     * @param int
     */
    function setBoardId($x){
        $this->boardId = $x;
    }
    
    /**
     * Creates a new column object in the db.
     * @global mysqli connection $mysqli
     */
    function create(){
        global $mysqli;
        
        $stmt = $mysqli->prepare("INSERT INTO catalyteretro_column (name, boardId) VALUES (?, ?)");
        $stmt->bind_param("si", $this->name, $this->boardId);
        $stmt->execute();
        $stmt->close();
    }
    
    /**
     * Returns all column objects belonging to a board.
     * @global mysqli connection $mysqli
     * @return boolean|\column
     */
    function getColumnsByBoardId(){
        $columns = array();
        global $mysqli;
        
        $stmt = $mysqli->prepare("SELECT id, name FROM catalyteretro_column WHERE boardId = ?");
        $stmt->bind_param("i", $this->boardId);
        $stmt->execute();        
        $result = $stmt->get_result();
        $stmt->close();
        
        if(mysqli_num_rows($result) > 0){
            while($row = $result->fetch_assoc()) {
                $column = new column();
                $column->setId($row["id"]);
                $column->setName($row["name"]);
                $column->setBoardId($this->boardId);
                $columns[] = $column;
            }
            return $columns;
        }else{
            return false;
        }
    }
}
