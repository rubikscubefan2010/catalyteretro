<?php

/* 
 * Controller file which handles requests dealing with comments added to baords.
 */

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

include_once $_SERVER["DOCUMENT_ROOT"] . '/catalyteretro/engine.php';

if(isset($func)){
    $func();
}

/**
 * Handles request to add a comment to a specific retro board.
 * This is meant to be more of an api.
 */
function createComment(){
    $submittedComment = $_POST["comment"];
    $boardId = $_POST["boardId"];
    $columnId = $_POST["columnId"];
    
    $board = new board();
    $board->setId($boardId);
    $board->getBoardById();
    
    if(!$board->getLocked()){ //Only allow submission of comments to an unlocked board.
        $comment = new comment();
        $comment->setBoardId(intval($boardId));
        $comment->setColumnId(intval($columnId));
        $comment->setComment($submittedComment);

        $comment->create();
        echo true;
    }else{
        echo false; //Send message to front end for a failed comment submission.
    }
}


/**
 * Handles the submission of an up vote on a comment on the board.
 * Submits the updated vote count to the db.
 */
function voteUp(){
    $commentId = $_POST["commentId"];
    
    $comment = new comment();
    $comment->setId($commentId);
    $comment->getCommentById();
    
    $boardId = $comment->getBoardId();
    
    $board = new board();
    $board->setId($boardId);
    $board->getBoardById();
    
    if(!$board->getLocked()){ //Only allow up votes on an unlocked board.
        $currentVoteCount = $comment->getVoteCount();
        $currentVoteCount++;

        $comment->setVoteCount($currentVoteCount);

        $comment->updateVoteCount();
        echo true;
    }else{
        echo false; //Send message to front end for a failed  submission.
    }
        
}

/**
 * Server side code for HTML5 SSE.
 * This returns the most recent comments to the front end.
 */
function latestComment(){
    $comment = new comment();
    $comment->getLatestComment();
    
    echo "data: {\"msg\": \"".addcslashes($comment->getComment(), '"')."\", \"id\":".$comment->getColumnId().", \"commentId\":".$comment->getId()."}  \n\n";
    flush();
}


/**
 * HTML SSE for outputing the latest votecount for a comment from the db.
 */
function latestVoteCount(){
    //Get session array of comments.
    $commentsOnBoardView = $_SESSION["commentsArray"];
    
    //Create array of just comment ids and vote counts
    $idVoteCounts = array();
    foreach($commentsOnBoardView as $cobv){
        $idVoteCounts[$cobv->getId()] = $cobv->getVoteCount();
    }
    
    //Get all comments via the ids from the model (or board id?).
    $comment = new comment();
    $comment->setBoardId($_SESSION["boardId"]);
    $commentsOnBoard = $comment->getAllCommentsByBoard();
    
    //Create array of ids and votecounts from the db
    $dbVoteCounts = array();
    foreach($commentsOnBoard as $cob){
        $dbVoteCounts[$cob->getId()] = $cob->getVoteCount();
    }
    //Run a comparison between the vote counts on the session array and the returned model comments.
    $diff = array_diff($dbVoteCounts, $idVoteCounts);
    
    //echo out the differences as JSON
    echo "data: {\"msg\":".json_encode($diff)."} \n\n";
    
    flush();
    
    //update the session array.
    //IS THIS NEEDED OR DOES IT CAUSE A BUG?
//    $_SESSION["commentsArray"] = $commentsOnBoard;
}


/**
 * Puts all the comment data from an up vote into session.
 */
function addToVoteArray(){
    //Take in comment data: id.
    $commentId = $_POST["cId"];
    
    //Create comment from id.
    $comment = new comment();
    $comment->setId($commentId);
    $comment->getCommentById();
    
    //Add comment to session array.
    $_SESSION["commentsArray"][] = $comment;
}
