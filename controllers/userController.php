<?php

/* 
 * Controller file which handles requests dealing with users.
 */
include_once $_SERVER["DOCUMENT_ROOT"] . '/catalyteretro/engine.php';

if(isset($func)){
    $func();
}



/**
 * Takes in new user information and uses the model to create a new user in the database.
 * 
 * TODO: Forward new user to login page with success message.
 */
function registerUser(){
    unset($_SESSION["user"]);
    unset($_SESSION["error"]);
    
    $user = new user();
    $user->setEmail($_POST["email"]);
    $uniqueEmail = $user->uniqueEmailCheck();
    
    if($uniqueEmail){
        //do not allow registration
        $_SESSION["error"] = "User email already exists.";
        echo getView("userRegister.php", null);
    }else{
        //allow registration
        $user->setPassword($_POST["password"]);
        $user->setFname($_POST["fname"]);
        $user->setLname($_POST["lname"]);
        $user->create();
        echo getView("userLogin.php", null);
    }  
}

/**
 * Takes in submitted credentials.
 * Verifies user is real.
 * Assigns login token for site security
 * Puts user object in session
 * 
 * TODO: Assign a proper landing page after login.s
 */
function loginUser(){
    $email = $_POST["email"];

    $user = new user();
    $user->setEmail($email);
    
    if(!$user->login()){
        echo false;
    }else{
       $_SESSION["user"] = $user->getId();
       $_SESSION["token"] = $email;
    }
    
    echo getView("userDashboard.php", NULL);
    
}

/**
 * Logs out the users.
 * Unsets session variables and redirects user to the login page.
 */
function logoutUser(){
    global $gClient;
    unset($_SESSION["user"]);
    session_destroy();
    
    // Remove token and user data from the session
    unset($_SESSION['token']);
    unset($_SESSION['userData']);
    // Reset OAuth access token
    $gClient->revokeToken();
    
    echo true;
}

