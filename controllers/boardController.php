<?php

/* 
 * Controller file handling requests dealing with retro boards.
 */

include_once $_SERVER["DOCUMENT_ROOT"] . '/catalyteretro/engine.php';

if(isset($func)){
    $func();
}


/**
 * Handles a request to create a new board for a user.
 * Redirects to the view of the board and sends the boardId as a uri parameter.
 */
function createBoard(){
    unset($_SESSION["error"]);
    
    $board = new board();
    $board->setName($_POST["boardName"]);
    $board->setUserId($_SESSION["user"]);
    $uniqueBoard = $board->uniqueBoardNameCheck();
    
    //Checks if the user already has a board with the same name.
    if($uniqueBoard){
        //do not allow board
        $_SESSION["error"] = "That board already exists.";
        echo getView("userDashboard.php", null);
    }else{
        //allow board
        $board->setUserId($_SESSION["user"]);
        $board->create();
        
        $boardId = $board->getId();

        $column = new column();
        $column->setBoardId($boardId);
        $column->setName($_POST["firstColumn"]);
        $column->create();

        $column->setName($_POST["secondColumn"]);
        $column->create();

        $column->setName($_POST["thirdColumn"]);
        $column->create();     

        //User should end up at the new board.
        forward("board?boardId=".$boardId);
    }   
}

/**
* Endpoint to update the model of the board with a new locked state.
*/
function lockBoard(){
   $boardId = $_POST["boardId"];

   $board = new board();
   $board->setId($boardId);

   $board->getBoardById();

   if($board->getLocked()){
       $board->setLocked(false);
   }else{
       $board->setLocked(true);
   }

   $board->boardLock();

   echo $board->getLocked();
}