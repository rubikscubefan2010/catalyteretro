/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function onSignIn(googleUser){
    var profile = googleUser.getBasicProfile();
    email = profile.getEmail();
    $.ajax({
        type: "POST",
        url: "login",
        data: {email: email},
        success: function (e) {
            if(e){
                window.location.href = "dashboard";
            }else{
                alert("Something went wrong.  Please try again.");
            }
        }
    });
}

/**
* Calls the sign out end point to revoke the access granted by the user.
* 
*/
function signOut() {
   var auth2 = gapi.auth2.getAuthInstance();
   auth2.signOut().then(function () {

       $.ajax({
           type: "POST",
           url: "logout",
           data: {},
           success: function (e) {
               if(e){
                   window.location = "loginUser";
               }else{
                   console.log("Not Signed out.");
               }
           }
       });
   });
}

/**
* Sends board lock state to backend api.
*/
function updateLockedState(boardId){
   var lockButton = document.getElementById(boardId);

   $.ajax({
       type: "POST",
       url: "lockBoard",
       data: {boardId: boardId},
       success: function (e){
           if(e){
               //change button text
               lockButton.innerHTML = "Unlock";
           }else{
               //change button text
               lockButton.innerHTML = "Lock";
           }
       }
   });
}

/**
* Toggles the view of the board form on the page between hidden and visible.
*/
function showBoardForm() {
   var x = document.getElementById("formDiv");
   if (x.style.display == "none") {
     x.style.display = "block";
   } else {
     x.style.display = "none";
   }
}


/**
* Copies current url to the clipboard.
* Alerts user the action has occured.
*/       
function copyLink() {
 var dummy = document.createElement('input'),
  text = window.location.href;

  document.body.appendChild(dummy);
  dummy.value = text;
  dummy.select();
  document.execCommand('copy');
  document.body.removeChild(dummy);
  alert("URL copied to clipbaord.  Send it to others so they can participate.");
}


 /**
* Sends a call to an endpoint to up vote a comment.
* @param int commentId
*/
function upVote(commentId){
    
   $.ajax({
       type: "POST",
       url: "voteUp",
       data: {commentId: commentId},
       success: function (e) {   
        if(!e){
            alert("This board has been locked by the owner.  No new votes may be added at this time.");
        }else{
            var count = document.getElementById("com"+commentId).innerHTML;
            count++;
            document.getElementById("com"+commentId).innerHTML = count;
        }
      }
   });
}

/**
 * Prevents submission of create board form if fields are blank.
 * @returns {Boolean}
 */
function validateBoardForm()
{
    var a=document.forms["newBoardForm"]["boardName"].value.trim();
    var b=document.forms["newBoardForm"]["firstColumn"].value.trim();
    var c=document.forms["newBoardForm"]["secondColumn"].value.trim();
    var d=document.forms["newBoardForm"]["thirdColumn"].value.trim();
    if (a==null || a=="",b==null || b=="",c==null || c=="",d==null || d=="")
    {
        alert("Please Fill All Required Field");
        return false;
    }
}